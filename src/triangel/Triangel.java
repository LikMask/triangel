//******************************************************************
// Programmerare: Robin Will�n wilrob-2
// Datum: 2016-02-06
// Senast uppdaterad: 2016-02-09 Robin Will�n
// Beskrivning: This Applications prints a triangle based on the users input.
// Version: 0.1
//******************************************************************

package triangel;

import java.util.Scanner;

public class Triangel {

	public static void main(String[] args) {

	//Declaring variables
	int theNumber = 0;			//The variable determining how long/big the triangle is.
	int upOrDown = 0;			//The variable controlling if the triangle should be printed (user input).
	int theCounter = 0;			//The variable controlling the first loop.
	int theCounterCounter = 0;	//The variable controlling the second loop within the first loop.
	
	Scanner in = new Scanner(System.in);
	
	while(theNumber >= 0)	//Endless loop (the loop/application exit whit System.exit(0)).
	{
		do
		{
			do
			{
				//Input from user how long the triangle should be. 
				System.out.print("Ange l�ngen p� de 2 lika l�nga sidorna (Avsluta med -1): ");
				theNumber = in.nextInt();
				
				//Check if the user whant's to exit the program.
				if (theNumber == -1)
				{
					System.out.println("Hej d�");
					System.exit(0);
				}
				else if (theNumber >= 10)	//Error output.
				{
					System.out.println("Error, sidorna m�ste vara mindre �n 10");
				}
				else if (theNumber <= -2)	//Error output.
				{
					System.out.println("Error, sidorna kan inte vara negativa(om du vill avsluta ange -1");
				}
			}
			while(theNumber <= -2);		//Control that the user don't enter a invalid value.
		}
		while(theNumber >= 10);			//Control that the user don't enter a invalid value.
		//Input from user if the angel should be down or up.
		System.out.print("Ska den r�ta vinkeln vara ned�t (0) eller upp�t (1): ");
		upOrDown = in.nextInt();
		
		//Loops that prints the triangle.
		if(upOrDown == 0)
		{
			theCounter = 1;
			while(theCounter <= theNumber )
			{
				
				theCounterCounter = 1;
				while (theCounterCounter <= theCounter)
				{
					System.out.print(theCounter);
					theCounterCounter++;
				}
				System.out.println("");
				theCounter++;
				
			}
		}
		else if(upOrDown == 1)
		{			
			theCounter = theNumber;
			while(theCounter >= 1)
			{
				theCounterCounter = theCounter;
				while (theCounterCounter >= 1)
				{
					System.out.print(theCounter);
					theCounterCounter--;
				}
				System.out.println("");
				theCounter--;	
			}
		}
		else
		{
			System.out.println("Error: V�rdet kan bara vara 0 eller 1");
		}
	}
	in.close(); //Never used, but is the for the aesthetics.
	}
}
